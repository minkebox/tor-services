FROM alpine:latest

RUN apk --no-cache add tor miniupnpd inotify-tools dnsmasq

COPY root/ /

ENTRYPOINT ["/startup.sh"]
