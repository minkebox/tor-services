#! /bin/sh

LEASES=/leases/upnp.leases
SERVICES=/var/lib/tor/services

SERVICE_INTERFACE=${__SECONDARY_INTERFACE}
if [ "${SERVICE_INTERFACE}" = "" ]; then
  SERVICE_INTERFACE=${__DEFAULT_INTERFACE}
fi

trap "killall inotifywait tor miniupnpd sleep; exit" TERM INT

chmod 600 /etc/tor/torrc.tmpl
chmod 700 /var/lib/tor
cp /etc/tor/torrc.tmpl /etc/tor/torrc

# Minimum needed for miniupnpd
iptables -t nat -N MINIUPNPD

# Monitoring network we use to attach to our Tor services. We switch TX and RX as anything
# we receive from a service is being transmitted out, and vice versa.
iptables -I OUTPUT -o ${SERVICE_INTERFACE} -j RX
iptables -I INPUT -i ${SERVICE_INTERFACE} -j TX

# Masquarade outgoing traffic (not Tor incoming)
if [ "${__SECONDARY_INTERFACE}" != "" ]; then
  iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT -i ${__SECONDARY_INTERFACE}
  iptables -t nat -A POSTROUTING -j MASQUERADE -o ${__DEFAULT_INTERFACE}
fi

update() {
  cp /dev/null /tmp/services
  cat /etc/tor/torrc | grep "HiddenServiceDir" | sed "s:^HiddenServiceDir /var/lib/tor/hidden_services/::" | while read id ; do
    if [ -d "/var/lib/tor/hidden_services/${id}" ]; then
      echo "[\"${id}\",\"$(cat /var/lib/tor/hidden_services/${id}/hostname)\"]" >> /tmp/services
    fi
  done
  echo "[$(cat /tmp/services | paste -s -d,)]" > ${SERVICES}
}

update_websites() {
  for website in ${WEBSITES}; do
    enabled=$(echo $website | cut -d"#" -f 1)
    ip=$(echo $website | cut -d"#" -f 2)
    port=$(echo $website | cut -d"#" -f 3)
    id=$(echo $website | cut -d"#" -f 4)
    if [ "$enabled" = "true" ]; then
      mkdir -m 700 -p "/var/lib/tor/hidden_services/${id}"
      echo "HiddenServiceDir /var/lib/tor/hidden_services/${id}" >> /etc/tor/torrc
      echo "HiddenServicePort 80 ${ip}:${port}" >> /etc/tor/torrc
    fi
  done
}

update_leases() {
  cat ${LEASES} | while read line; do
    protocol=$(echo $line | cut -d':' -f1)
    port=$(echo $line | cut -d':' -f2)
    ip=$(echo $line | cut -d':' -f3)
    id=$(echo $line | cut -d':' -f6-9 | sed "s/ /_/g")
    if [ "$protocol" = "TCP" ]; then
      mkdir -m 700 -p "/var/lib/tor/hidden_services/${id}"
      echo "HiddenServiceDir /var/lib/tor/hidden_services/${id}" >> /etc/tor/torrc
      echo "HiddenServicePort ${port} ${ip}:${port}" >> /etc/tor/torrc
    fi
  done
}

cat > /etc/miniupnpd.conf <<__EOF__
ext_ifname=lo
listening_ip=${SERVICE_INTERFACE}
enable_natpmp=yes
enable_upnp=yes
min_lifetime=120
max_lifetime=86400
lease_file=${LEASES}
secure_mode=no
system_uptime=yes
notify_interval=60
uuid=6574C473-47BB-4C5A-9BF6-FAE7130DE229
serial=1
__EOF__
touch ${LEASES}
echo '[]' > ${SERVICES}

cp /etc/tor/torrc.tmpl /etc/tor/torrc
update_websites
update_leases

/usr/bin/tor &
# Only run upnp if we're a gateway and we're not listening to the main network
if [ "${__SECONDARY_INTERFACE}" != "" -a "${SERVICE_INTERFACE}" != "${__DHCP_INTERFACE}" ]; then
  /usr/sbin/miniupnpd
fi
/usr/sbin/dnsmasq

(touch ${LEASES} ; inotifywait --quiet --monitor ${LEASES} --event close_write | while read event; do
  #echo $event
  cp /etc/tor/torrc.tmpl /etc/tor/torrc
  update_websites
  update_leases
  killall -HUP tor
done) &

(inotifywait --recursive --quiet --monitor /var/lib/tor/hidden_services --event move | while read event; do
  #echo $event
  update
done) &
update

sleep 2147483647d &
wait "$!"
